import { IData} from '../datasources/data-access'

import { Request } from 'express'
import {DAO} from '../datasources/data-dao'
import  { PROFILE_CONSTANT } from '../errorhandling/constant'


const AWS = require('aws-sdk');
// AWS.config.update({ region: 'us-east-1' });
AWS.config.update({
   accessKeyId: "AKIAR4E36UWI32JMQFML",
   secretAccessKey: "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",
   region:"us-east-1", 
  })
const sns = new AWS.SNS();
// const axios = require('axios');

export async function snsAlertReceiver(event: Request) {

    let type = event.headers["x-amz-sns-message-type"];
    let body = JSON.parse(event.body)
  
    if (type == "SubscriptionConfirmation") {
      let params = {
        TopicArn: event.headers["x-amz-sns-topic-arn"],
        Token: body.Token
      };

        sns.confirmSubscription(params, function (err: { stack: any; }, data: { SubscriptionArn: string; }) {
            if (err) {
                console.log(err.stack);
                console.log({ "Message": "Subscription confirmation for https failed" })


            }

            else {
                console.log(`Subscription confirmed for https endpoint ` + JSON.stringify(data));

                console.log({ "Message": "Subscription confirmed with ARN " + data.SubscriptionArn })


            }

        });
    }

    if (type == 'Notification') {
        console.log(event.body)
        let body = JSON.parse(event.body)
        let rawObject = body.Message;

        if (typeof (rawObject) != 'object') {
            rawObject = JSON.parse(rawObject)
        }
        // Create param based on the type received
        let eventType = body.MessageAttributes.event_type.Value;
        if (eventType.includes("deleteProfile")) {
            let dataAccess: IData;

            dataAccess = new DAO();

            let result = await dataAccess.deleteProfileFromDynamo(rawObject.id);
            if (result) {
                console.log({ Message: "Deleting Profile Successful" });
            } else {
                console.log({ ERROR: `Deleting Profile id: ${rawObject.id} failed` });
            }
        }
        if (eventType.includes("updatePhotoUrl")) {
            let dataAccess: IData;
      
            dataAccess = new DAO();
      
            let result = await dataAccess.updatePhotoUrl(rawObject.photoUrl,rawObject.profileId);
            if (result) {
              console.log({ Message: "Update photoUrl successful" });
            } else {
              console.log({ ERROR: `Update photoUrl id: ${rawObject.photoUrl,rawObject.profileId } failed` });
            }
          }
      if (eventType.includes("banProfile")) {
        let dataAccess: IData;

        dataAccess = new DAO();
       console.log('ban profile inside snsAlertReceiver')
        let result = await dataAccess.banProfileByAdminInDynamo(rawObject.id);
        if (result) {
          console.log({ Message: PROFILE_CONSTANT.PROFILE_BANNED });
        } else {
          console.log({ ERROR: `Banning Profile id: ${rawObject.id} failed` });
        }
      }
    }
}
