export enum ProfilePrivacy {
    PRIVATE = 'V',
    PUBLIC = 'P'
}

export enum ProfileGender {
    MALE = 'M',
    FEMALE = 'F',
    OTHER = 'O'
}
