import { IUser } from '../datasources/data-access'
import { Field, ObjectType,Directive } from 'type-graphql'

@Directive(`@key(fields: "id ")`)
@ObjectType()
export class User implements IUser {

    @Field(() => String)
    id: string;

    @Field({nullable: true})
    screenName: string;

    @Field({nullable: true})
    firstName: string;

    @Field({nullable: true})
    lastName: string;

    @Field({nullable: true})
    privacy: string;

    @Field({nullable: true})
    photoUrl: string;

    @Field({nullable: true})
    status: string;

    @Field({nullable: true})
    shortBio: string;

    @Field({nullable: true})
    gender: string;

    @Field({nullable:true})
    email: string

    @Field({nullable:true})
    phone: string

    @Field({nullable:true})
    yearOfBirth: number

    @Field({nullable:true})
    firebaseId: string

    @Field(() => [String], { nullable: true })
    interests: string[]

}

