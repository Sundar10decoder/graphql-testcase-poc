import { Field,  InputType } from "type-graphql";
import { User } from "../types/User";
import { IsEnum, Length, Min, Max, IsEmail  } from 'class-validator'
import { ProfileGender, ProfilePrivacy } from '../types/Enum';

@InputType({ description: "Unique ID of a User" })
export class UserInputId implements Partial<User> {

    @Field(() => String)    
    @Length(20, 40)  
    id: string;
}

@InputType({ description: "New  User data" })
export class UserInputData implements Partial<User> {

    @Field()
    @Length(2, 25)
    screenName: string;

    @Field()
    @Length(1, 35)
    firstName: string;

    @Field({nullable: true})
    @Length(1, 40)
    lastName: string;

    @Field({nullable: true})
    @Length(7, 20)
    phone: string;

    @Field({nullable: true})
    @Min(1930)
    @Max(2020)
    yearOfBirth: number

    @Field()
    @IsEnum(ProfilePrivacy)
    privacy: string;

    @Field({ nullable: true })
    @Length(1, 160)
    shortBio: string;
        
    @Field()
    @IsEnum(ProfileGender)    
    gender: string;

    @Field(() => String, {nullable: true})
    @Length(20, 36)
    id: string;

    @Field()
    firebaseId: string;

    @Field()
    photoUrl: string;

    @Field(() => String)
    @IsEmail()
    email: string;

    @Field(() => [String], { nullable: true })
    interests: string[]


}

@InputType({ description: "Pagination" })
export class Pagination {

    @Field()      
    limit: number;

    @Field()
    id: string;
}
