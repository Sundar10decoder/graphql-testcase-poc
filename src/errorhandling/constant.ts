export let errorName = {
    DUPLICATE_SCREENNAME: 'DUPLICATE_SCREENNAME',
    DUPLICATE_FIREBASEID: 'DUPLICATE_FIREBASEID',
    PROFILE_NOT_FOUND: "PROFILE_NOT_FOUND"
}

export let errorType = {
    DUPLICATE_SCREENNAME: {
        message: 'ScreenName already exist.',
        statusCode: 409,
    },
    DUPLICATE_FIREBASEID: {
        message: 'Account already in use.',
        statusCode: 409,
    },
    PROFILE_NOT_FOUND: {
        message: 'Profile does not exist',
        statusCode: 404,
    },
    ARGUMENT_VALIDATION: {
        message: 'Argument Validation Error',
        statusCode: 400,
    },
    UNKNOWN_ERROR: {
        message: 'Unknown error',
        statusCode: 500,
    }
}

export let PROFILE_CONSTANT = {
    PROFILE_DELETE: 'Profile Deleted',
    PROFILE_BANNED: 'Profile Banned',
}