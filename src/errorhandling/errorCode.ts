import { errorType } from './constant'

export let getErrorCode = (errorName: any) => {
    if (errorName === 'Argument Validation Error') {
        return errorType['ARGUMENT_VALIDATION']
    }
    else if (errorName === 'PROFILE_NOT_FOUND') {
        return errorType['PROFILE_NOT_FOUND']
    }
    else if (errorName === 'DUPLICATE_SCREENNAME'){
        return errorType['DUPLICATE_SCREENNAME']
    }
    else if (errorName === 'DUPLICATE_FIREBASEID'){
        return errorType['DUPLICATE_FIREBASEID']
    }
    else {
        return errorType['UNKNOWN_ERROR']
    }
    
}
