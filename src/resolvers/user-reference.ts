import { DAO } from "../datasources/data-dao";
import { User } from "../types/User";

export async function resolveUserReference(reference: Pick<User, "id">): Promise<User> {
    console.log("reference-profile",reference)
    const res: any = await new DAO().getProfileById({id:reference.id});
    return res

}
