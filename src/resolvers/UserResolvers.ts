import { User} from "../types/User";
import { Arg, Query, Resolver, Mutation, FieldResolver, Root} from "type-graphql";
import { IUser, IData } from '../datasources/data-access'
import { DAO } from '../datasources/data-dao'
import { UserInputId, Pagination, UserInputData } from '../input-types/UserInput'

@Resolver(() => User)
export class UserResolvers {

  private dataAccess: IData;

  constructor() {
    this.dataAccess = new DAO();
  }

  // @Query(() => User)
  // async getProfileById(@Arg("id") id: UserInputId): Promise<IUser> {
  //   console.log('Query getProfile ID' + id)
  //   return await this.dataAccess.getProfileById(id);
  // }

  // @Query(() => [User])
  async getProfileByIds(@Arg("id", () => [String]) id: [String]): Promise<IUser[]> {
    console.log('Query getProfile ID' + id)
    return await this.dataAccess.getProfileByIds(id);
  }

  @Query(() => [User])
  async getProfiles(@Arg("pagination") pagination: Pagination): Promise<IUser[]> {
    return await this.dataAccess.getProfiles(pagination);
  }

  // @Mutation(() => String)
  async deleteProfile(@Arg("id") id: UserInputId): Promise<String> {
    console.log('..', id)
    return await this.dataAccess.deleteProfile(id)
  }


  //Mutations
  //TODO: need to add SNS receiver for add user in graph. 
  @Mutation(() => User)
  async addUser(@Arg("data") userInputData: UserInputData): Promise<IUser> {
    return await this.dataAccess.addUser(userInputData)
  }

  //TODO: need to add SNS receiver for update user in graph. 
  @Mutation(() => User)
  async updateUser(@Arg("data") userInputData: UserInputData): Promise<IUser> {
    console.log('...', userInputData)
    return await this.dataAccess.updateProfile(userInputData)
  }
  // @Query(()=>[User])
  async getDeletedProfile(): Promise<IUser[]>{
    return this.dataAccess.getDeletedProfile();
  }

  @Query(() => Boolean)
  async checkScreenName(@Arg("screenName") screenName:string){
    console.log('Query getProfile ID' + screenName)
    return await this.dataAccess.checkScreenName(screenName);
  }

  @Query(() => [User])
  async getBannedProfile(): Promise<IUser[]> {
    return await this.dataAccess.getBannedProfile();
  }

  @FieldResolver(() => String)
  async profileBannedByAdminFromDynamo(@Root() user: User): Promise<string> {
    if (user.id === null) {
      return "Profile does not exist"
    } else {
      return await this.dataAccess.banProfileByAdminInDynamo(user.id);
    }
  }

  //Temporary testing
  @Mutation(() => User)
  async addbanProfileDbToDynamoDBUser(@Arg("profileId") user: UserInputId): Promise<string> {
    return await this.dataAccess.banProfileDbToDynamoDB(user.id)
  }


}
