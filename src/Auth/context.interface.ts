import { IFirebeseUser } from "./firebase.user.interface";

export interface IContext {
  fbUser?: IFirebeseUser;
}