//the Firbase credentilas for Xposure

import { AuthenticationError, ForbiddenError } from "apollo-server-express";
import { IFirebeseUser } from "./firebase.user.interface";

//import serviceAccount from './firebase/xposure-tech-firebase-adminsdk-p7i2h-b0766c353a.json'

const serviceAccount = require('./firebase/xposure-tech-firebase-adminsdk-p7i2h-b0766c353a.json');
const firebase = require("firebase-admin");

firebase.initializeApp({
    projectId: "xposure-tech",
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://xposure-tech.firebaseio.com"
});

class FbUser implements IFirebeseUser {
    fbid: string;
    jwt: string;
    roles: string[];
}

export async function authHandler(req:any){

  let token = req.headers["authorization"]
  if (!token || token == '') {
    throw new AuthenticationError('MUST be logged in'); 
  }

  token = removeBearer(token)

  if (!token || token == '') {
    throw new AuthenticationError('MUST be logged in'); 
  }
/*
  firebase.auth().verifyIdToken(token)
      .then(function (decodedToken:any) {
          req.uid = decodedToken.uid;
          req.admin = decodedToken.admin
      }).catch(function (error:any) {
          console.log(`invalid token in verifyIdToken :` + error);
          throw new ForbiddenError('NOT Authorized');
      });
*/
    let fbUser: IFirebeseUser = new FbUser();
    try {
        let decodedToken =  await firebase.auth().verifyIdToken(token);
        fbUser.fbid = decodedToken.uid;
        fbUser.jwt = token;
        fbUser.roles = (decodedToken.admin) ? ["ADMIN"]:[];

        req.user = fbUser;
    }
    catch(error:any) {
        console.log(`invalid token in verifyIdToken :` + error);
        throw new ForbiddenError('NOT Authorized');
    }
};

export function removeBearer(token: string): string {
  return token.replace("Bearer ", '')
}

