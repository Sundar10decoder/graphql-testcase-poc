export interface IFirebeseUser {
    fbid: string; //firebase Id
    jwt: string;
    roles: string[];
  }