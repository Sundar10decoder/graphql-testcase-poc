import { AuthChecker } from "type-graphql";
import { IContext } from "./context.interface";


// create auth checker function
export const authChecker: AuthChecker<IContext> = ({ context: { fbUser } }, roles) => {

  console.log(JSON.stringify(fbUser));

  if (roles.length === 0) {
    // if `@Authorized()`, check only is user exist
    return fbUser !== undefined;
  }
  // there are some roles defined now

  if (!fbUser) {
    // and if no user, restrict access
    return false;
  }

  if (fbUser.roles.some((role: string) => roles.includes(role))) {
    // grant access if the roles overlap\
    return true;
  }

  // no roles matched, restrict access
  return false;
};