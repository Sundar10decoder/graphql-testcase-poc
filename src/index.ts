import "reflect-metadata";
import { ApolloServer } from "apollo-server";

import { UserResolvers } from "./resolvers/UserResolvers";
import { dynamodb } from './db/dbConfig';

import { User } from "./types/User";
import { buildFederatedSchema } from "./helpers/buildFederatedSchema";
import { resolveUserReference } from "./resolvers/user-reference";
import Express from 'express'
import { httpHeader } from "./helpers/header";

import { GraphQLError } from "graphql";
import { getErrorCode } from "./errorhandling/errorCode";
var bodyParser = require('body-parser')
import { snsAlertReceiver } from "./helpers/snsAlertReceiver"


const app = Express();

export const main = async () => {

  const schema = await buildFederatedSchema(
    {
      resolvers: [UserResolvers],
      orphanedTypes: [User],
    },
    {
      User: { __resolveReference: resolveUserReference },
    },
  );

  dynamodb();

  const apolloServer = new ApolloServer({
    schema,
    tracing: false,
    playground: true,
    context: ({ req }) => ({
      header: httpHeader(req.headers.header)
    }),
    formatError: (err:GraphQLError) => {
      if (err.message === 'Argument Validation Error') {
        let data: any = { ...err.originalError }
        let dataError = { ...data.validationErrors }
        let validationField: any = {}
        validationField = {...dataError[0]}
        let error = getErrorCode(err.message)
        return ({
         extensions: { code: error.statusCode, serviceName: null, query: null, variables: null, },
          message: error.message,
          field: validationField.constraints
        })
      } 
      else {
        let error = getErrorCode(err.message)
        return ({
          extensions: { code: error.statusCode, serviceName: null, query: null, variables: null },
          message: error.message
        })
      }
    }
  });

  app.get("/healthCheck", (_req, res) => {
    console.log('Inside the healthCheck')
    res.status(200).send({
      message: 'Profile service successfully UP'
    })
  });
  app.use(bodyParser.text())
  app.route('/profile').post(snsAlertReceiver)

  await app.listen(4012);
  const { url } = await apolloServer.listen(5012);
  console.log(`Profile service ready at ${url}`);

  module.exports=app;

};
main();

