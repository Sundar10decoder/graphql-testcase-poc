import { IData, IUser } from './data-access'
import { UserInputId, UserInputData, Pagination } from '../input-types/UserInput'
import ProfileModel from '../db/profileModel'
import { v4 as uuidv4 } from 'uuid'
import DeletedProfileModel from '../db/deletedProfileModel'
import { globalConfig } from '../config';
import { header } from '../helpers/header'
import  {errorName, PROFILE_CONSTANT } from '../errorhandling/constant'
import BannedProfileModel from '../db/bannedProfileModel';

const AWS = require('aws-sdk');
// AWS.config.update({ region: 'us-east-1' });
AWS.config.update({
    accessKeyId: "AKIAR4E36UWI32JMQFML",
    secretAccessKey: "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",
    region:"us-east-1", 
})
const sns = new AWS.SNS();


//Must be global object. Else evertime the value is reset
let exclusiveStartKey = {
    id: '1'
}

export class DAO implements IData {

    async getProfileById(uid: UserInputId): Promise<any> {
        try {
            const res: Object = await ProfileModel.get({ id: uid.id });
            console.log('Profile service result ', res)
            if(res == null) throw new Error(errorName.PROFILE_NOT_FOUND)
            else return res
        } 
        catch(err) {
            return err
        }        
    }

    async getProfileByIds(UserIds: [String]): Promise<any> {
        try {
            let profileid: String = JSON.stringify(UserIds).replace(/'/g, '').replace(/\\/g, '').replace(/""/g, '"').replace(/,/g, ' ').replace(/"/g, '').replace(/\[/g, '').replace(/\]/g, '')
            let profileids = profileid.split(" ")
            const res: Object = await ProfileModel.batchGet(profileids)
            return res
        }
        catch(err) {
            return err
        }
    }

    async getProfiles(pagination: Pagination): Promise<any> {
        
        let count: number = 0;
        let userList = [];
        try {
            if (pagination.id === '') {
                userList = await ProfileModel.scan().limit(pagination.limit).exec();
                exclusiveStartKey = userList.lastKey
                count = userList.count;

            } else {
                exclusiveStartKey.id = pagination.id
                //get from database where you left off 
                userList = await ProfileModel.scan().limit(pagination.limit).startAt(exclusiveStartKey).exec();
                count = userList.count;
                exclusiveStartKey.id = '1'
            }
            let result = {
                "status": true,
                "userList": userList,
                "count": count
            };
            let res = result.userList
            return res
        }
        catch(err) {
            return err
        }
    }

    async addUser(data: UserInputData): Promise<any> {
        data.id = uuidv4()
        let inputScreenName = { ...data }.screenName
        let inputFirebaseId = {...data}.firebaseId
        //let user: any = {}
        try {
            //const res = await ProfileModel.scan("screenName").eq(`${check}`).exec()
            const checkScreenName = await this.checkScreenName(inputScreenName)
            console.log("Check screenName: ", checkScreenName)
            if (checkScreenName ) {
                throw new Error(errorName.DUPLICATE_SCREENNAME)
            }
            const checkFirebaseId = await this.checkFirebaseId(inputFirebaseId)
            console.log("Check firebaseId: ", checkFirebaseId)
            if (checkFirebaseId ) {
                throw new Error(errorName.DUPLICATE_FIREBASEID)
            }
            else {
                const user = await ProfileModel.create({ ...data }).then(async (result: any) => {
                    console.log(header)
                    let deviceToken: any = ""
                    if (typeof header != 'object') {
                        deviceToken = JSON.parse(header).devicetoken;
                    }
                    console.log("device", deviceToken)
                    let message = JSON.stringify({
                        id: result.id,
                        firebaseId: result.firebaseId,
                        privacy: result.privacy,
                        screenName: result.screenName,
                        photoUrl: result.photoUrl,
                        deviceId: uuidv4(),
                        deviceToken: deviceToken
                    })

                    console.log("creating message for sending to topic")
                    const pParams = {
                        Message: message,
                        TopicArn: globalConfig.TOPIC_ARN,
                        MessageAttributes: {
                            'event_type': {
                                'DataType': 'String',
                                'StringValue': 'postProfile'
                            }
                        }
                    };
                    let publishTextPromise = sns.publish(pParams).promise();
                    publishTextPromise.then(
                        (data: { MessageId: string; }) => {
                            console.log(`Message ${pParams.Message} sent to the topic ${pParams.TopicArn}`);
                            console.log('MessageID is ' + data.MessageId);

                        }).catch(
                            function (err: { stack: any; }) {
                                console.log(err.stack);
                            }
                        );
                    return result
                })
                delete user.createdAt
                delete user.updatedAt
                return user
            }
        }
        catch(err) {
            return err
        }
    }

    async updateProfile(data: UserInputData): Promise<any> {
        let uid = data.id;
        let data1: any = data;
        delete data1.id
        try{
            await ProfileModel.update({ id: uid }, { ...data1 })
            const res = await ProfileModel.get({ id: uid }).then(async (result: any) => {
            let message = JSON.stringify({
                profileId: result.id,
                firebaseId: result.firebaseId,
                privacy: result.privacy,
                screenName: result.screenName,
                photoUrl: result.photoUrl
            })
            console.log("creating message for sending to topic")
            const pParams = {
                Message: message,
                TopicArn: globalConfig.TOPIC_ARN,
                MessageAttributes: {
                    'event_type': {
                        'DataType': 'String',
                        'StringValue': 'updateProfile'
                    }
                }
            };
            let publishTextPromise = sns.publish(pParams).promise();
            publishTextPromise.then(
                (data: { MessageId: string; }) => {
                    console.log(`Message ${pParams.Message} sent to the topic ${pParams.TopicArn}`);
                    console.log('MessageID is ' + data.MessageId);
                }).catch(
                    function (err: { stack: any; }) {
                        console.log(err.stack);
                    }
                );
            return result
        })
        return res
        }
        catch(err) {
            return err
        }        
    }

    async deleteProfile(uid: UserInputId): Promise<String> {
        try {
            await ProfileModel.delete({ id: uid.id })
            let res = PROFILE_CONSTANT.PROFILE_DELETE
            return res
        }
        catch(err) {
            return err
        }        
    }

    async Profile(uid: string): Promise<any> {
        try { 
            const res = await ProfileModel.get({ id: uid })
            return res
        }
        catch(err) {
            return err
        }       
    }

    async deleteProfileFromDynamo(profileId: string): Promise<string> {
        try{
            
            let message: string = await ProfileModel.get({ id: profileId }).then(async (profileDetail) => {
                if(profileDetail===undefined){
                    return "Profile deleted successfully"
                }else{
    
                let profile: any = {}
                profile = profileDetail
                delete profile.createdAt
                delete profile.updatedAt
                await DeletedProfileModel.create({ ...profile })
                return await ProfileModel.delete({ id: profileId }).then(async () => {
                    return PROFILE_CONSTANT.PROFILE_DELETE
                }) 
            }   
            })
            return message
        }
        catch(err) {
            return err
        }        
    }

    async getDeletedProfile(): Promise<IUser[]> {
        try{
            const deletedProfile: Object = await DeletedProfileModel.scan().exec();
            return <IUser[]><Object>deletedProfile
        }
        catch(err) {
            return err
        }        
    }


    async checkScreenName(screenName: string): Promise<any> {
        let check = await ProfileModel.scan().where("screenName").eq(screenName).attributes(["screenName"]).exec()
        if ((check).length > 0) {
            return true
        } else {
            return false
        }
    }

    async checkFirebaseId(firebaseId: string): Promise<any>{
        let check = await ProfileModel.scan().where("firebaseId").eq(firebaseId).attributes(["firebaseId"]).exec()
        if ((check).length > 0){
            return true
        } else {
            return false
        }
    }

    async updatePhotoUrl(photoUrl: string,id:string):Promise<any>{
        const profile = await ProfileModel.update({id:id},{"photoUrl":photoUrl})
        delete profile.createdAt
        delete profile.updatedAt
        return profile
        }
    
    async banProfileByAdminInDynamo(profileId: string): Promise<string> {
        try {
            console.log('banProfileByAdminInDynamo function',profileId)
            let message: string = await ProfileModel.get({ id: profileId }).then(async (profileDetail) => {
                console.log('get profileID',profileDetail)
                delete profileDetail.createdAt
                delete profileDetail.updateAt
                return await BannedProfileModel.create({ ...profileDetail }).then(async (bannedProfileDetails) => {
                    console.log('created banned profile model',bannedProfileDetails)
                    return await ProfileModel.delete({ id: profileId }).then(async () => {
                        console.log('profile id deleted')
                        console.log(PROFILE_CONSTANT.PROFILE_BANNED)
                        return PROFILE_CONSTANT.PROFILE_BANNED
                    })

                })
            })
            return message
        }
        catch (err) {
            console.log("Error during, ban profile in dynamo: ", err)
            return err
        }


    }


    async getBannedProfile(): Promise<IUser[]> {
        console.log('inside getBannedProfile function')
        const deletedMedia: Object = await BannedProfileModel.scan().exec();
        return <IUser[]><Object>deletedMedia
    }

 //temporary testing
    async banProfileDbToDynamoDB(profileId: string): Promise<string> {
        try {
            console.log('banProfileByAdminInDynamo function')
            let message: string = await BannedProfileModel.get({ id: profileId }).then(async (profileDetail) => {
                console.log('get profileID',profileDetail)
                delete profileDetail.createdAt
                delete profileDetail.updateAt
                return await ProfileModel.create({ ...profileDetail }).then(async () => {
                    console.log('created banned profile model')
                    return await BannedProfileModel.delete({ id: profileId }).then(async () => {
                        console.log('banProfile id deleted')
                        console.log(PROFILE_CONSTANT.PROFILE_BANNED)
                        return "Profile created in profileModel"
                    })

                })
            })
            return message
        }
        catch (err) {
            console.log("Error during, ban profile in dynamo: ", err)
            return err
        }


    }
}



