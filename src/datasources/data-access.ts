import { UserInputId, UserInputData, Pagination } from '../input-types/UserInput'

export interface IUser {
    id: string;
    screenName: string;
    firstName: string;
    privacy: string;
    shortBio: string;
    photoUrl: string;
    status: string;
    gender:string,
    email:string,
    phone: string,
    yearOfBirth: number,
    lastName: string,
    firebaseId:string,
    interests: string[]

};

export interface IData {
    getProfileById(userId:UserInputId): Promise<IUser>;
    getProfiles(pagination: Pagination): Promise<IUser[]>;
    getProfileByIds(userIds: [String]): Promise<IUser[]>
    deleteProfile(userId: UserInputId): Promise<String>
    Profile(userId: String): Promise<IUser>;

    //Mutation
    addUser(data: UserInputData): Promise<IUser>
    updateProfile(data: UserInputData): Promise<IUser> 

    deleteProfileFromDynamo(profileId: string): Promise<string>
    getDeletedProfile():Promise<IUser[]>
    checkScreenName(screenName: string): Promise<string>

    updatePhotoUrl(photoUrl: string,id:string):Promise<IUser>
    banProfileByAdminInDynamo(profileId: string): Promise<string>
    getBannedProfile(): Promise<IUser[]>

    //Temporary testing
    banProfileDbToDynamoDB(profileId: string): Promise<string>

}