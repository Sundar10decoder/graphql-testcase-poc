import * as dynamoose from 'dynamoose';

export const schema = new dynamoose.Schema(
  {
    id: {
      type: String,
      hashKey: true,
    },
    screenName: String,
    firstName: String,
    lastName: String,
    gender:String,
    email:String,
    shortBio: String,
    privacy: String,
    phone: String,
    yearOfBirth: Number,
    firebaseId: String,
    photoUrl:String,
    "interests": {
      "type": Array,
      "schema": [String]
    },

  },
  {
    timestamps: true,
  }
);

const ProfileModel = dynamoose.model('Profile', schema, {
   "create": true,
  // number of request to read and write at same time
  throughput: {
    read: 5,
    write: 5,
  },
});
export default  ProfileModel ;