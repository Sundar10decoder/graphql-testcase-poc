const url = `http://localhost:5005/`;
const request = require('supertest')(url);

//const supertest =require('supertest');

//const request=supertest("http://localhost:5005/")

describe("getMovieById Test case", ()=>{

  test("fetch by id", async () => {

     request
        .post("/")
        .send({
          query: `{ getProfiles(pagination: {limit:3,id:""}){ id,firstName,gender } }`,
        })
        .set("Accept", "application/json")
        .set("Content-Type", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end(function (err, res) {
          if (err){  console.log("error",err);}
          if(res.body.errors != null){
            expect(res.body.errors[0].message).toBe("Cannot return null for non-nullable field Query.getMovieById.")
          }
          else{
            expect(res.body.data.getProfiles).toBeInstanceOf(Array);
            expect(res.body.data.getProfiles[0]).toBeInstanceOf(Object);
            expect(Object.keys(res.body.data.getProfiles[0]).length).toBe(3);
            expect(res.body.data.getProfiles[0].id).toEqual("e4e4cfb6-3655-4fca-be4f-586b0615634d");
            expect((res.body.data.getProfiles).length).toBe(3);
          }
      });
    });
    it("fetch by id", async () => {

      request
         .post("/")
         .send({
           query: `{ getProfiles(pagination: {limit:3}){ id,firstName,gender } }`,
         })
         .set("Accept", "application/json")
         .set("Content-Type", "application/json")
         .expect(400)
         .end(function (err, res) {
           if (err){  console.log("error",err);}
           if(res.body.errors != null){
             expect(res.body.errors[0].message).toEqual("Unknown error")
           }
           else{
             expect(res.body.data.getProfiles).toBeInstanceOf(Array);
             //res.body.data.getProfiles.should.have.lengthOf(3)
           }
       });
     });
});