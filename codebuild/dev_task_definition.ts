module.exports = {

    "requiresCompatibilities": [
        "FARGATE"
    ],
    "executionRoleArn": `arn:aws:iam::${process.env.AWS_ACCOUNT_ID}:role/ecsTaskExecutionRole`,
    "containerDefinitions": [
        {
            "name": "x2profile",
            "image": `${process.env.AWS_ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/profile-service:${process.env.IMAGE_TAG}`,
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/x2profile",
                    "awslogs-region": "us-east-1",
                    "awslogs-stream-prefix": "ecs"
                }
            },
            "environment": [
                {
                    "name": "AWS_ACCESS_KEY",
                    "value": `${process.env.AWS_ACCESS_KEY}`
                },
                {
                    "name": "AWS_SECRET_KEY",
                    "value": `${process.env.AWS_SECRET_KEY}`
                },    
                {
                    "name": "TOPIC_ARN",
                    "value": "arn:aws:sns:us-east-1:129176085905:bgnotifyGql"
                }
            ],
            "cpu": 0,

            "memoryReservation": 500,
            "essential": true,
            "portMappings": [
                {
                    "hostPort": 4014,
                    "protocol": "tcp",
                    "containerPort": 4014
                }
            ]
        }
    ],
    "memory": "1024",
    "taskRoleArn": `arn:aws:iam::${process.env.AWS_ACCOUNT_ID}:role/ecsTaskExecutionRole`,
    "cpu": "256",
    "networkMode": "awsvpc",
    "family": "x2profile"

};

var ConfigurationJSON = JSON.stringify(module.exports);
const config = module.exports;

const jsonConfig = JSON.parse(JSON.stringify(ConfigurationJSON))

console.log(jsonConfig)