module.exports = {
  preset: 'ts-jest',
  setupFilesAfterEnv: ['./setup.js'],
  testEnvironment: 'node',
    moduleNameMapper: {
    "@exmpl/(.*)": "<rootDir>/src/$1"
  },
};