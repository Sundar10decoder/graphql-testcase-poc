FROM node:15.13.0-alpine3.10
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY ./dist .
EXPOSE 5005
CMD [ "node", "index.js" ]